{-# LANGUAGE DeriveGeneric #-}
module Lib where
import GHC.Generics
import Dhall

import Lib.UDA
import Lib.UDAV
import Lib.Report
import Lib.Context
import Lib.Alias
import Lib.Urgency

import Test.Hspec
import Test.QuickCheck


data Config = Config 
  { dataLocation :: String
  , bulk :: Natural
  , searchCaseSensitive :: Bool
  , uda :: [UDA]
  , udaValues :: [UDAV]
  , report :: [Report]
  , context :: [Context]
  , alias :: [Alias]
  , urgency :: [Urgency]
  } deriving (Generic)
    
instance FromDhall Config 

tests = do
  describe "A feature" $ do
    it "can do some things" $ do
      1 `shouldBe` 1

{-# LANGUAGE DeriveGeneric #-}
module Lib.UDAV where

import GHC.Generics
import Dhall

data UDAV = UDAV
    { name :: String
    , values :: String
    } deriving (Generic, Show)

instance FromDhall UDAV
    
udavToString :: UDAV -> String
udavToString udaValues = unlines ["uda." ++ name udaValues ++ ".values=" ++ values udaValues]
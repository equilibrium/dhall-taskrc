{-# LANGUAGE DeriveGeneric #-}
module Lib.UDA where

import GHC.Generics
import Dhall


data UDA = UDA
    { name :: String
    , dataType :: String
    , label :: String
    } deriving (Generic, Show)
instance FromDhall UDA

udaToString :: UDA -> String
udaToString uda = unlines ["uda." ++ name uda ++ value |
    value <-
     [ ".type=" ++ dataType uda
     , ".label=" ++ label uda
     ]
    ]
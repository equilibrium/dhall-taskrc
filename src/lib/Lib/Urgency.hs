{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Lib.Urgency where

import GHC.Generics
import Dhall
import Data.Function((&))

class Coefficient a where
    name :: a -> String
    coefficient :: a -> Integer

data SimpleCoefficient = SimpleCoefficient
    deriving (Generic, Coefficient)
data Priority = Priority
    { values :: [SimpleCoefficient] }
    deriving (Generic, Coefficient)
data Urgency = Urgency
    { due :: Integer
    , scheduled :: Integer
    , blocking :: Integer
    , tags :: [SimpleCoefficient]
    , uda :: [Priority]
    } deriving (Generic)

instance FromDhall SimpleCoefficient
instance FromDhall Priority
instance FromDhall Urgency

urgencyToString :: Urgency -> String
urgencyToString urgency = unlines ["urgency." ++ value |
    value <- [ ".due=" ++ (urgency & due & show)
     , ".scheduled=" ++ (urgency & scheduled & show)
     , ".blocking=" ++ (urgency & blocking & show)
     --, urgency & tags ++ coefficient urgency.tags
     --, "uda." ++ name urgency.uda ++ name urgency.uda.values ++ "coefficient=" ++ coefficient urgency.uda.values
     ]
    ]
{-# LANGUAGE DeriveGeneric #-}
module Lib.Context where

import GHC.Generics
import Dhall

data Context = Context
    { name :: String
    , filter :: String
    } deriving (Generic, Show)
instance FromDhall Context

contextToString :: Context -> String    
contextToString context = "context." ++ name context ++ ".filter=" ++ Lib.Context.filter context
    
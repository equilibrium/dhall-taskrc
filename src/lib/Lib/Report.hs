{-# LANGUAGE DeriveGeneric #-}
module Lib.Report where

import GHC.Generics
import Dhall


data Report = Report
    { name :: String
    , description :: String
    , columns :: String
    , labels :: String
    , filter :: String
    , sort :: String
    } deriving (Generic, Show)
instance FromDhall Report

reportToString :: Report -> String    
reportToString report = unlines ["report." ++ name report ++ value |
    value <- [".description=" ++ description report
     , ".columns=" ++ columns report
     , ".labels=" ++ labels report
     , ".filter=" ++ Lib.Report.filter report
     , ".sort=" ++ sort report]
    ]


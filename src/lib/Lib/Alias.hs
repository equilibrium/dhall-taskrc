{-# LANGUAGE DeriveGeneric #-}
module Lib.Alias where

import GHC.Generics
import Dhall

data Alias = Alias
    { name :: String
    , value :: String
    } deriving (Generic, Show)
instance FromDhall Alias

aliasToString :: Alias -> String    
aliasToString alias = "alias." ++ name alias ++ ".value=" ++ value alias
    
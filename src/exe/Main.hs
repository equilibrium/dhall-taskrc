{-# LANGUAGE OverloadedStrings #-}
module Main where

import Lib
import Lib.UDA
import Lib.Report
import Lib.Context
import Lib.Alias
import Data.Vector(toList)
import Data.Function((&))
import Dhall

generateRc :: Config -> String
generateRc config =
    (("bulk=" ++ (config & bulk & show)):
     ("search.case.sensitive=" ++ (config & searchCaseSensitive & show)):
     ("data.location=" ++ (dataLocation config)):
     (config & uda & Prelude.map udaToString) ++
     (config & report & Prelude.map reportToString) ++
     (config & context & Prelude.map contextToString) ++
     (config & alias & Prelude.map aliasToString)) & unlines

main :: IO ()
main = do
    config <- input auto "./res/taskrc.dhall" :: IO Config
    config & generateRc & putStrLn


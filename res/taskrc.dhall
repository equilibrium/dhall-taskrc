let Urgency = { name: Text, coefficient: Integer }

in {

dataLocation = "~/.local/share/task",

bulk = 5,
searchCaseSensitive = False,

alias = [
  { name = "cfg", value = "add +pc +config" },
  { name = "research", value = "add +pc +research" },
  { name = "waiton", value = "mod +waitfor wait:1w due:3w" }
],

uda = [
  {
    name = "url",
    dataType = "string",
    label = "URL"
  },
  {
    name = "recurDue",
    dataType = "duration",
    label = "RecDue"
  },
  {
    name = "recurWait",
    dataType = "duration",
    label = "RecWait"
  }
],
udaValues = [
  { name = "priority", values = "H,M,,L" }
],

urgency = {
  due = +7,
  scheduled = +5,
  blocking = +3,
  tags = [
    { name = "next", coefficient = +9 },
    { name = "config", coefficient = -2 },
    { name = "reflect", coefficient = -3 }
  ],
  uda = [
    {
      name = "priority",
      coefficient = +0,
      values = [
        { name = "H", coefficient = +5 },
        { name = "M", coefficient = +2 },
        { name = "L", coefficient = -2 }
      ]
    },
    {
      name = "recurDue",
      coefficient = -3,
      values = [] : List Urgency
    }
  ]
},

context = [
  { name = "dev", filter = "+dev" },
  { name = "work", filter = "+work" },
  { name = "fog", filter = "+comm or +admin or +move or +config or +browse" }
],

report = [
  {
    name = "inbox",
    description = "Relevant tasks without project",
    columns = "id,priority,tags,scheduled.countdown,due.relative,description.desc,url,urgency",
    labels = "ID,Prio,Tags,⏰,Description,Url,Urg",
    filter = "status:pending project: -config -consume",
    sort = "urgency-"
  },
  {
    name = "next",
    description = "",
    columns = "id,depends,priority,tags,scheduled.relative,due.relative,project,description.count,urgency",
    labels = "ID,Deps,Prio,Tags,,⏰,Proj,Description,Urg",
    filter = "status:pending limit:20",
    sort = ""
  }
]

}

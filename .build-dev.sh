#!/usr/bin/env sh

project="$(cat *.cabal | grep executable | cut -d' ' -f2)"
cabal build $project && $(find dist-newstyle -type f -name $project)
